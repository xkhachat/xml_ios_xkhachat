//
//  ViewController.swift
//  Project
//
//  Created by Aleksandre Khachaturov on 14/04/2018.
//  Copyright © 2018 MENDELU. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


class ViewController: UIViewController, UISearchBarDelegate, CLLocationManagerDelegate {
    

    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBAction func searchButton(_ sender: Any)
    {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        present(searchController, animated: true, completion: nil)
    }
    
    
//Search function Starts here//
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        //Ignore user
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        //Activity Indicator
        let activityInd = UIActivityIndicatorView()
        activityInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        activityInd.center = self.view.center
        activityInd.hidesWhenStopped = true
        activityInd.startAnimating()
        
        self.view.addSubview(activityInd)
        
        //Hide search bar
        searchBar.resignFirstResponder()
        dismiss(animated: true, completion: nil)
        
        //Create the search request
        let search = MKLocalSearchRequest()
        search.naturalLanguageQuery = searchBar.text
        
        let activeSearch = MKLocalSearch(request: search)
        
       
// Search function ended ///


    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
   
//** My current location end **//
    
    //---Save Button---
    @IBAction func saveButton(_ sender: Any) {

            if (self.latitude == nil || self.longitude == nil) {
                    print("nil---")
                self.present(alert, animated: true, completion: nil)
            } else {
               
 
                let latitude:CLLocationDegrees = self.latitude!
                let longitude:CLLocationDegrees = self.longitude!
                
                let regionDistance:CLLocationDistance = 1000;
                let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
                let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
                
                let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center), MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)]
                
                let placemark = MKPlacemark(coordinate: coordinates)
                let mapItem = MKMapItem(placemark: placemark)
                mapItem.name = self.date
                mapItem.openInMaps(launchOptions: options)
              
            }
            
         }

}





